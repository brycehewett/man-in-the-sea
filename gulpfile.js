'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const include = require("gulp-include");
const uglify = require('gulp-uglify');
const plumber = require('gulp-plumber');
const rename = require("gulp-rename");
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;

let sass_src = './stylesheets/sass/';
let css_dest = './';
let js_src = './js/';
let js_dest = './js/';

// browser-sync task for starting the server.
gulp.task('browser-sync', () => {
    //watch files
    let files = [
    './style.css',
    './js/*.js',
    './**/*.php'
    ];

    //initialize browsersync
    browserSync.init(files, {
    //browsersync with a php server
    proxy: "localhost/maninthesea",
    notify: true
    });
});

gulp.task('sass', () => {
  return gulp.src(sass_src + 'style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'compressed',
      includePaths: [
        './node_modules/breakpoint-sass/stylesheets',
        './node_modules/susy/sass',
        './node_modules/modern-normalize',
        './libs/Ionicons/scss'
      ]
    }).on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(css_dest))
    .pipe(reload({
      stream:true
    }));
});

gulp.task('js', () => {
  return gulp.src(js_src + 'functions.js')
  .pipe(plumber())
  .pipe(include({
      includePaths: [
        './node_modules'
      ]
    }))
  .pipe(uglify())
  .pipe(rename({ suffix: '.min' }))
  .pipe(gulp.dest(js_dest))
});

gulp.task('default', ['js', 'sass', 'browser-sync'], () => {
    gulp.watch(sass_src + "**/*.{sass,scss}", ['sass']);
    gulp.watch(js_src  + "functions.js", ['js']);
});
