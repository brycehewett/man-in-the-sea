      <?php
        $msm_google_analytics_ua = get_option( 'msm_google_analytics_ua' );
        $footer_menu_settings = array(
          'menu'            => 'footer-menu',
          'theme_location'  => 'footer-menu',
          'container'       => '',
          'echo'            => true,
          'fallback_cb'     => false,
          'items_wrap'      => '<nav class="footer__menu"><ul>%3$s</ul></nav>'
        );
      ?>
      <?php if (!is_404()) {?>
        <footer class="footer">
          <div class="container">
            <?php wp_nav_menu( $footer_menu_settings );?>

            <div class="footer__logo__column">
              <img src="<?php bloginfo('template_url'); ?>/images/institute-of-diving-logo.png"/>
            </div>
            <div class='footer__widget__column'>
              <?php dynamic_sidebar( 'footer_column_1' ); ?>
            </div>
            <div class='footer__widget__column'>
              <?php dynamic_sidebar( 'footer_column_2' ); ?>
            </div>
            <div class='footer__widget__column last'>
              <?php dynamic_sidebar( 'footer_column_3' ); ?>
            </div>
          </div>
          <div class="container">
            <small class="footer__copyright">
              The Man in the Sea Museum is a 501(c)(3) organization</br>
              Copyright 2012-<?php echo date("Y"); echo " &copy; "; bloginfo('name'); ?></small>
          </div>
        </footer>
      <?php } ?>

      <?php wp_footer(); ?>

      <?php
        if ( $msm_google_analytics_ua ) {
            echo "<!-- Global site tag (gtag.js) - Google Analytics -->
                    <script async src='https://www.googletagmanager.com/gtag/js?id=". $msm_google_analytics_ua ."'></script>
                    <script>
                      window.dataLayer = window.dataLayer || [];
                      function gtag(){dataLayer.push(arguments);}
                      gtag('js', new Date());

                      gtag('config', '". $msm_google_analytics_ua ."');
                    </script>";

        }?>

    </div>
  </body>
</html>
