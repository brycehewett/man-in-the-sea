<?php get_header(); ?>

  <div class="container content">
    <h1><?php _e('404','MSM'); ?></h1>
    <h2><?php _e('Page Not Found','MSM'); ?></h2>
    <a href="/" class="btn__default btn__white btn__large"><?php _e('Return to Home Page','MSM'); ?></a>
	</div>

<?php get_footer(); ?>
