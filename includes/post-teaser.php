<article>
  <div <?php post_class('post-teaser page-content') ?>>
      <span class="post-teaser__post-date"><time datetime="<?php echo date(DATE_W3C); ?>" pubdate class="updated"><?php the_time('F j, Y') ?></time></span>
      <h3 class="post-teaser__header"><a href="<?php the_permalink() ?>"><?php short_title(); ?></a></h3>
      <span class="post-teaser__meta"><?php the_category(' ') ?></span>
      <?php if ( has_post_thumbnail() ) { ?>
        <div class="post-teaser__thumbnail">
          <a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>"><?php the_post_thumbnail('large'); ?></a>
        </div>
      <?php } ?>

      <span class="post-teaser__author">
        <?php if(function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), '58' );} ?> by <?php the_author_posts_link() ?>
      </span>

      <div class="post-teaser__excerpt"><?php the_excerpt(); ?></div>
    <div class="post-teaser__bottom">
      <a class="comment-link" href="<?php the_permalink(); ?>#comments">View Comments</a> |
      <a class="article-link" href="<?php the_permalink(); ?>">Read More <i class="ionicons ion-ios-arrow-thin-right"></i></a>
    </div>

  </div>
</article>
