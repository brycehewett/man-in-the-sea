<header>
  <div class="header">
    <div class="container no-padding logo-container">
      <a class="logo" href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a>
    </div>
    <div class="secondary-top-nav">
      <div class="container no-padding">
        <?php $defaults = array(
          'menu'            => 'secondary-menu',
          'theme_location'  => 'secondary-menu',
          'container'       => '',
          'echo'            => true,
          'fallback_cb'     => false,
          'items_wrap'      => '<ul class="secondary-nav">%3$s</ul>',
          'depth'           => 1,);
        wp_nav_menu( $defaults );

        if (is_woocommerce_activated()) {?>
          <div class="cart-nav">
            <?php
              include('nav-cart.php');
              the_widget( 'WC_Widget_Cart', 'title=' );
            ?>
          </div>
        <?php } ?>
      </div>
    </div>
    <div class="top-nav">
      <div class="container no-padding">
        <?php $defaults = array(
          'menu'            => 'primary-menu',
          'theme_location'  => 'primary-menu',
          'container'       => '',
          'echo'            => true,
          'fallback_cb'     => false,
          'items_wrap'      => '<ul class="primary-nav">%3$s</ul>',
          'depth'           => 3,);
        wp_nav_menu( $defaults );

        if (is_woocommerce_activated()) {?>
          <div class="mobile-cart-nav">
            <?php include('nav-cart.php'); ?>
          </div>
          <?php } ?>
        <div class="menu-toggle"><i class="icon ion-navicon-round"></i></div>
      </div>
    </div>

    <?php if (is_home()) {
      include('home/home-header.php');
      include('home/home-header-secondary.php');
    } else {} ?>
  </div>
</header>
