<div class="home__secondary-header-content">
  <div class="secondary-header-content secondary-header-content--column-one">
    <div class="text-wrapper">
      <h3>Contribute to Ongoing Restoration of SEALAB I and other Dive Relics</h3>
      <a href="/sealab-I" class="btn__default btn__white btn__allcaps">Learn More&nbsp;&nbsp;<i class="icon ion-chevron-right"></i></a>
    </div>
  </div>
  <div class="secondary-header-content secondary-header-content--column-two">
    <div class="text-wrapper">
      <h3>Plan Your Visit</h3>
      <a href="/plan-your-visit" class="btn__default btn__white btn__allcaps">Learn More&nbsp;&nbsp;<i class="icon ion-chevron-right"></i></a>
    </div>
  </div>
</div>
