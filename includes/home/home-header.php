<div class="home__header-content">
  <h1>Discover the Underwater World</h1>
  <div class="text-wrapper">
    <p>Man in the Sea Museum is a great place to explore fascinating exhibits and get acquainted with the roots of diving.</p>
  </div>
  <div class="button-wrapper">
    <a class="btn__large btn__white btn__allcaps" href="/tour-exhibits/">Tour Exhibits&nbsp;&nbsp;<i class="icon ion-chevron-right"></i></a>
    <a class="btn__large btn__white btn__allcaps" href="/memberships/">Become A Member&nbsp;&nbsp;<i class="icon ion-chevron-right"></i></a>
  </div>
</div>
