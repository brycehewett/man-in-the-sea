<span class="nav-shopping-cart">
  <?php global $woocommerce;
  $cart_has_items = $woocommerce->cart->cart_contents_count > 0 ? 'cart-has-items' : 'cart-empty';?>

  <a class="cart-link" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>">
    <i class="icon ion-android-cart"></i><?php echo $woocommerce->cart->get_cart_total(); ?>
    <span class="shopping-cart-item-count <?php print $cart_has_items ?>">
      <?php echo sprintf(_n('%d <span class="cart-count-text">item</span>', '%d <span class="cart-count-text">items</span>', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?>
    </span>
    <i class="icon ion-chevron-down"></i>
  </a>
</span>
