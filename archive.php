<?php get_header(); ?>

	<div class="container narrow">
		<?php $breadcrumb_args = array(
				'wrap_before' => '<div class="crumbs">',
				'wrap_after' => '</div>',
				'delimiter' => ' / '
			);woocommerce_breadcrumb( $breadcrumb_args ); ?>

		<?php if (have_posts()) : ?>

			<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

			<?php /* If this is a category archive */ if (is_category()) { ?>
				<h1 class="page-title"><?php single_cat_title(); ?></h1>

			<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
				<h1 class="page-title">Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h1>

			<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
				<h1 class="page-title">Archive for <?php the_time('F jS, Y'); ?></h1>

			<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
				<h1 class="page-title">Archive for <?php the_time('F, Y'); ?></h1>

			<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
				<h1 class="page-title">Archive for <?php the_time('Y'); ?></h1>

			<?php /* If this is an author archive */ } elseif (is_author()) { ?>
				<h1 class="page-title">Author Archive</h1>

			<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
				<h1 class="page-title">Blog Archives</h1>

			<?php } ?>

			<?php while (have_posts()) : the_post();

				include('includes/post-teaser.php');

					endwhile;

				include (TEMPLATEPATH . '/includes/post-nav.php' );

					else : echo '<h2>Not Found</h2>';

				endif;?>

	</div>

<?php get_footer(); ?>
