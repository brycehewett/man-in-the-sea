<?php
/*
Template Name: Blog Template
*/
?>
<?php get_header(); ?>

<div class="container narrow">
  <?php $breadcrumb_args = array(
      'wrap_before' => '<div class="crumbs">',
      'wrap_after' => '</div>',
      'delimiter' => ' / '
    );woocommerce_breadcrumb( $breadcrumb_args ); ?>

  <h1 class="page-title"><?php _e('Recent Articles','MSM'); ?></h1>
  <?php query_posts('posts_per_page='.get_option('posts_per_page').'&paged='. get_query_var('paged'));

    if( have_posts() ):
      while( have_posts() ): the_post();
      include('includes/post-teaser.php');
      endwhile;
      include('includes/post-nav.php');
    else: ?>

    <div class="post-404 noposts">
      <p><?php _e('No Posts Found','MSM'); ?></p>
    </div><!-- /.post-404 -->

  <?php endif; wp_reset_query(); ?>
</div>

<?php get_footer(); ?>
