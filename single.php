<?php get_header(); ?>

  <div class="container narrow">
    <span class="post__post-date"><time datetime="<?php echo date(DATE_W3C); ?>" pubdate class="updated"><?php the_time('F j, Y') ?></time></span>

    <h1 class="post__page-title page-title"><?php the_title(); ?></h1>

    <span class="post__meta"><?php the_category(' ') ?></span>
    <div class="page-content">
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <article <?php post_class('post') ?>>



          <?php if ( has_post_thumbnail() ) { ?>
            <div class="post__thumbnail">
              <a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>"><?php the_post_thumbnail('large'); ?></a>
            </div>
          <?php } ?>

          <div class="post__entry-content">
            <?php the_content(); ?>
          </div><!--.entry-content-->

          <footer>
            <div class="post__meta">
              <?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
              <?php the_tags( 'Tags: ', ' ', ''); ?>
              <?php $shortlink = wp_get_shortlink(); ?>
            </div>
            <div class="share-links">
              <a class="share-links__twitter" title="Tweet This" target="_blank" href="http://twitter.com/home?status=<?php echo str_replace("|", "", get_the_title()); echo(' '); echo $shortlink; ?>"><i class="ionicons ion-social-twitter"></i>Tweet This</a>

              <a class="share-links__facebook" title="Share This" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&t=<?php the_title(); ?>" target="_blank"><i class="ionicons ion-social-facebook"></i>Share This</a>
            </div>
          </footer>

          <?php edit_post_link('Edit this entry','',''); ?>

          <div class="post__author">
            <?php if(function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), '58' ); /* This avatar is the user's gravatar (http://gravatar.com) based on their administrative email address */  } ?>

            <div id="author-description">
              <p>Written by <?php the_author_posts_link() ?></p>
            </div>
          </div><!--#post-author-->

          <?php include('includes/post-nav.php'); ?>

          <div class="post__respond" id="respond">
            <?php comments_template(); ?>
          </div><!--#respond-->

        </article>
      <?php endwhile; endif; ?>
    </div>
	</div>
</div>

<?php get_footer(); ?>
