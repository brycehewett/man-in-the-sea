<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="container narrow">
  <header>
    <?php $breadcrumb_args = array(
        'wrap_before' => '<div class="crumbs">',
        'wrap_after' => '</div>',
        'delimiter' => ' / '
      );woocommerce_breadcrumb( $breadcrumb_args ); ?>

    <h1 class="page-title"><?php the_title(); ?></h1>
    <?php if (has_excerpt()) {?>
      <h4 class="page-subtitle"><?php echo get_the_excerpt(); ?></h4>
    <?php }; ?>

  </header>
  <article class="page-content" id="post-<?php the_ID(); ?>">
    <?php the_content(); ?>
  </article>
</div>
<?php endwhile; endif; ?>

<?php get_footer(); ?>
