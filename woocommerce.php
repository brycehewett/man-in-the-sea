<?php get_header(); ?>

  <div class="container narrow ">
    <?php $breadcrumb_args = array(
        'wrap_before' => '<div class="crumbs">',
        'wrap_after' => '</div>',
        'delimiter' => ' / '
      );woocommerce_breadcrumb( $breadcrumb_args ); ?>

      <h1 class="page-title"><?php
        if (is_single()) {
          the_title();
        } else if (is_tax()) {
          single_term_title();
        } else {
          echo 'Shop';
        }?>
      </h1>

      <div class="page-content woo-content">
        <?php woocommerce_content(); ?>
      </div>
  </div>
</div>

<?php get_footer(); ?>
