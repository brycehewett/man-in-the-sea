<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
  <label class="screen-reader-text hidden" for="s">Type and hit enter to search…</label>
      <input type="text" value="" title="Site Search" name="s" id="search" placeholder="Type and press enter to search..." />
      <input type="submit" id="searchsubmit" style="display:none;" value="Search" />
</form>
