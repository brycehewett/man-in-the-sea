<!DOCTYPE html>

<!--[if lt IE 7 ]> <html class="ie ie6 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->

<head>

	<meta charset="<?php bloginfo('charset'); ?>">

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<?php if (is_search()) { ?>
		<meta name="robots" content="noindex, nofollow" />
	<?php } ?>

	<title><?php wp_title( '-', true, 'right' ); ?></title>

	<meta name="title" content="<?php wp_title( '-', true, 'right' ); ?>">
	<meta name="author" content="<?php bloginfo('name'); ?>">
	<meta name="Copyright" content="Copyright <?php bloginfo('name'); echo date(" Y"); ?>. All Rights Reserved.">

	<!--  Mobile Viewport meta tag
	j.mp/mobileviewport & davidbcalhoun.com/2010/viewport-metatag
	device-width : Occupy full width of the screen in its current orientation
	initial-scale = 1.0 retains dimensions instead of zooming out if page height > device height
	maximum-scale = 1.0 retains dimensions instead of zooming in if page width < device width-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<!--[if lt IE 9]>
		<script src="<?php bloginfo( 'template_url' ); ?>/libs/html5shiv/dist/html5shiv.min.js"></script>
		<script src="<?php bloginfo( 'template_url' ); ?>/libs/html5shiv/dist/html5shiv-printshiv.min.js"></script>
	<![endif]-->

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

	<?php wp_head(); ?>


	<?php if ( get_option('msm_facebook_app_id') ) {

				$facebook_app_id = get_option('msm_facebook_app_id');
				$default_img = get_option('site_icon');
		 		$thumb = get_post_meta($post->ID,'_thumbnail_id',false);
				if ($thumb) {
					$thumb = wp_get_attachment_image_src($thumb[0], 'thumbnail', false);
					$thumb = $thumb[0];
				}

			if ( ! empty( $facebook_app_id ) ) {

				echo '<meta property="fb:app_id" content="'.$facebook_app_id.'"/>';

			if ( is_single() || is_page()) { ?>

				<meta property="og:type" content="article" />
				<meta property="og:title" content="<?php single_post_title(''); ?>" />
				<meta property="og:description" content="<?php
					while(have_posts()):the_post();
					$out_excerpt = str_replace(array("\r\n", "\r", "\n"), "", get_the_excerpt());
					echo apply_filters('the_excerpt_rss', $out_excerpt);
					endwhile; 	?>" />
				<meta property="og:url" content="<?php the_permalink(); ?>"/>
				<meta property="og:image" content="<?php if ( $thumb[0] == null ) { echo $default_img; } else { echo $thumb; } ?>" />
				<?php  } else { ?>
					<meta property="og:type" content="article" />
					<meta property="og:title" content="<?php bloginfo('name'); ?>" />
					<meta property="og:url" content="<?php bloginfo('url'); ?>"/>
					<meta property="og:description" content="<?php bloginfo('description'); ?>" />
		    	<meta property="og:image" content="<?php  if ( $thumb[0] == null ) { echo $default_img; } else { echo $thumb; } ?>" />
				<?php	}

				}
  	} ?>

	</head><body <?php body_class();?>>
		<?php if (!is_404()) {
			include('includes/responsive-nav.php');
		} ?>

	<div class="site-wrapper">

	<?php

		if ( get_option('msm_facebook_app_id') ) {
			echo '<div id="fb-root"></div><script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId='.$facebook_app_id.'"; fjs.parentNode.insertBefore(js, fjs);	}(document, "script", "facebook-jssdk"));</script>';
		}

		if (!is_404()) {
			include('includes/navigation.php');
		}
	?>
