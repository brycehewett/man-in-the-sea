<?php

	// Add RSS links to <head> section
	add_theme_support( 'automatic-feed-links' );

	add_action('get_header', 'remove_admin_login_header');
	function remove_admin_login_header() {
		remove_action('wp_head', '_admin_bar_bump_cb');
	}

  function load_dependencies() {
      //Styles
			wp_enqueue_style( 'material-design-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons' );
			wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css' );

			//Scripts in the header
      wp_register_script('jQuery', ('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'),'1.11.1', false);

			//Scripts in the footer
			$msm_google_maps_api_key = get_option( 'msm_google_maps_api_key' );

			if ($msm_google_maps_api_key) {
				wp_register_script('map-api', ('https://maps.googleapis.com/maps/api/js?key=' . $msm_google_maps_api_key),false, false);
			}

      wp_register_script( 'custom-functions', get_template_directory_uri() . '/js/functions.min.js', '1.0', true );

			wp_enqueue_script('jQuery');
			wp_enqueue_script('map-api');
			wp_enqueue_script('custom-functions');
  }

  add_action( 'wp_enqueue_scripts', 'load_dependencies' );

	add_action( 'after_setup_theme', 'woocommerce_support' );
	function woocommerce_support() {
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
	}

	// Remove each style one by one
	add_filter( 'woocommerce_enqueue_styles', 'jk_dequeue_styles' );
	function jk_dequeue_styles( $enqueue_styles ) {
		unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
		return $enqueue_styles;
	}

	// Or just remove them all in one line
	add_filter( 'woocommerce_enqueue_styles', '__return_false' );

	/**
	* Breadcrumbs
	*/

	include_once( 'includes/breadcrumb.php' );

	// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php).
	// Used in conjunction with https://gist.github.com/DanielSantoro/1d0dc206e242239624eb71b2636ab148
	add_filter('add_to_cart_fragments', 'woocommerce_add_to_cart_fragments');

	add_post_type_support( 'page', 'excerpt' );

	function woocommerce_add_to_cart_fragments( $fragments ) {
		ob_start();
		include('includes/nav-cart.php');

		$fragments['span.nav-shopping-cart'] = ob_get_clean();

		return $fragments;
	}

	if ( ! function_exists( 'is_woocommerce_activated' ) ) {
		function is_woocommerce_activated() {
			if ( class_exists( 'woocommerce' ) ) { return true; } else { return false; }
		}
	}

	// Clean up the <head>
	function removeHeadLinks() {
    	remove_action('wp_head', 'rsd_link');
    	remove_action('wp_head', 'wlwmanifest_link');
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');

    if (function_exists('register_sidebar')) {
    	register_sidebar(array(
    		'name' => __('Sidebar Widgets','MSM' ),
    		'id'   => 'sidebar-widgets',
    		'description'   => __( 'These are widgets for the sidebar.','MSM' ),
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h3>',
    		'after_title'   => '</h3>'
    	));
    }

    // custom menu support
    add_theme_support( 'menus' );
    if ( function_exists( 'register_nav_menus' ) ) {
        register_nav_menus(
            array(
              'primary-menu' => __('Primary Menu', 'MSM'),
							'secondary-menu' => __('Secondary Menu', 'MSM'),
              'footer-menu' => __('Footer Menu', 'MSM'),
							'mobile-nav' => __('Mobile Nav', 'MSM')
            )
        );
    }

    add_theme_support( 'post-thumbnails' );

		//Register sidebars and widgetized areas.
		function init_custom_widgets() {
			register_sidebar( array(
				'name'          => 'Footer Column 1',
				'id'            => 'footer_column_1',
				'before_widget' => '<div class="widget">',
				'after_widget'  => '</div>',
				'before_title'  => '<h6>',
				'after_title'   => '</h6>',
			));

			register_sidebar( array(
				'name'          => 'Footer Column 2',
				'id'            => 'footer_column_2',
				'before_widget' => '<div class="widget">',
				'after_widget'  => '</div>',
				'before_title'  => '<h6>',
				'after_title'   => '</h6>',
			));

			register_sidebar( array(
				'name'          => 'Footer Column 3',
				'id'            => 'footer_column_3',
				'before_widget' => '<div class="widget">',
				'after_widget'  => '</div>',
				'before_title'  => '<h6>',
				'after_title'   => '</h6>',
			) );

			class msm_contact_info_widget extends WP_Widget {
				public function __construct() {
					$widget_ops = array(
						'classname' => 'msm_contact_info_widget',
						'description' => 'Displays contact information defined in Settings > General',
					);
					parent::__construct( 'msm_contact_info_widget', 'Contact Info', $widget_ops );
				}

				public function widget( $args, $instance ) {
					$msm_twitter_url = get_option( 'msm_twitter_url' );
					$msm_facebook_url = get_option( 'msm_facebook_url' );
					$msm_instgram_url = get_option( 'msm_instagram_url' );
					$msm_contact_email = get_option( 'msm_contact_email' );
					$msm_contact_phone = get_option( 'msm_contact_phone' );
					$msm_address = get_option( 'msm_address' );

					if ($msm_contact_email || $msm_contact_phone || $msm_address || $msm_twitter_url || $msm_facebook_url || $msm_instgram_url) {
						echo $args['before_widget'];
						echo '<div class="contact-widget">
										<ul>' .
											( $msm_contact_phone ? '<li class="contact-widget__phone"><a href="tel:'. $msm_contact_phone .'">'. $msm_contact_phone .'</a></li>' : '' ) .
											( $msm_contact_email ? '<li class="contact-widget__email"><a href="mailto:' . $msm_contact_email . '">' . $msm_contact_email . '</a></li>' : '' ) .
											( $msm_address ? '<li class="contact-widget__address">' . $msm_address . '</li>' : '' ) .
										'</ul>' .
										( $msm_twitter_url || $msm_facebook_url || $msm_instgram_url ? '<div class="contact-widget__social-links">' : '' ) .
											( $msm_twitter_url ? '<a class="contact-widget__social-links--twitter icon ion-social-twitter" href="' . $msm_twitter_url . '" target="_blank"></a>' : '' ) .
											( $msm_facebook_url ? '<a class="contact-widget__social-links--facebook icon ion-social-facebook" href="' . $msm_facebook_url . '" target="_blank"></a>' : '' ) .
											( $msm_instgram_url ? '<a class="contact-widget__social-links--instagram icon ion-social-instagram" href="' . $msm_instgram_url . '" target="_blank"></a>' : '' ) .
										( $msm_twitter_url || $msm_facebook_url || $msm_instgram_url ? '</div>' : '' ) .
									'</div>';
						echo $args['after_widget'];
					}
				}

				public function form( $instance ) {
					// outputs the options form on admin
					echo '<p>Displays contact information defined in <a href="' . admin_url( 'options-general.php' ) . '">Settings > General</a></p>';
				}
			}

			register_widget( 'msm_contact_info_widget' );
		}

		add_action( 'widgets_init', 'init_custom_widgets' );

    //add_theme_support( 'post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'audio', 'chat', 'video')); // Add 3.1 post format theme support.

    function wpe_excerptlength_index($length) {
        return 75;
    }
    function wpe_excerptmore($more) {
        return ' ...';
    }

    function wpe_excerpt($length_callback='', $more_callback='') {
        global $post;

    if(function_exists($length_callback)){
        add_filter('excerpt_length', $length_callback);
    }

    if(function_exists($more_callback)){
        add_filter('excerpt_more', $more_callback);
    }

        $output = get_the_excerpt();
        $output = apply_filters('wptexturize', $output);
        $output = apply_filters('convert_chars', $output);
        $output = '<p>'.$output.'</p>';
        echo $output;
    }

    function short_title() {
        $thetitle = get_the_title();
        $getlength = strlen($thetitle);
        $thelength = 45;

        echo substr($thetitle, 0, $thelength);

        if ($getlength > $thelength) echo "...";
    }

		function msm_customize_register( $wp_customize ) {
			$wp_customize->remove_section( 'custom_css' );
			$wp_customize->remove_section('static_front_page');
		}
		add_action( 'customize_register', 'msm_customize_register' );

		add_action('after_setup_theme', 'remove_admin_bar');

		function remove_admin_bar() {
			if (!current_user_can('administrator') && !is_admin()) {
			  show_admin_bar(false);
			}
		}

		function msm_settings_api_init() {
		 add_settings_field(
			 'msm_google_analytics_ua',
			 'Google Analytics',
			 'msm_google_analytics_ua_callback',
			 'general'
		 );
		 add_settings_field(
			 'msm_google_maps_api_key',
			 'Google Maps API Key',
			 'msm_google_maps_api_key_callback',
			 'general'
		 );
		 add_settings_field(
			 'msm_twitter_url',
			 'Twitter Url',
			 'msm_twitter_url_callback',
			 'general'
		 );
		 add_settings_field(
			 'msm_facebook_app_id',
			 'Facebook App ID',
			 'msm_facebook_app_id_callback',
			 'general'
		 );
		 add_settings_field(
			 'msm_facebook_url',
			 'Facebook Url',
			 'msm_facebook_url_callback',
			 'general'
		 );
		 add_settings_field(
			 'msm_instagram_url',
			 'Instagram Url',
			 'msm_instagram_url_callback',
			 'general'
		 );
		 add_settings_field(
			 'msm_contact_email',
			 'Contact Email',
			 'msm_contact_email_callback',
			 'general'
		 );
		 add_settings_field(
			 'msm_contact_phone',
			 'Contact Phone',
			 'msm_contact_phone_callback',
			 'general'
		 );
		 add_settings_field(
			 'msm_address',
			 'Address',
			 'msm_address_callback',
			 'general'
		 );
		 register_setting( 'general', 'msm_google_analytics_ua' );
		 register_setting( 'general', 'msm_google_maps_api_key' );
		 register_setting( 'general', 'msm_twitter_url' );
		 register_setting( 'general', 'msm_facebook_app_id' );
		 register_setting( 'general', 'msm_facebook_url' );
		 register_setting( 'general', 'msm_instagram_url' );
		 register_setting( 'general', 'msm_contact_email' );
		 register_setting( 'general', 'msm_contact_phone' );
		 register_setting( 'general', 'msm_address' );

		}

		add_action( 'admin_init', 'msm_settings_api_init' );

		function msm_google_analytics_ua_callback() {
		 echo '<input name="msm_google_analytics_ua" id="msm_google_analytics_ua" value="'. get_option( 'msm_google_analytics_ua' ) .'" type="text" class="regular-text" />';
		 echo '<p class="description" id="google-analytics-ua-description">UA-XXXXXX-XX</p>';
		}

		function msm_google_maps_api_key_callback() {
		 echo '<input name="msm_google_maps_api_key" id="msm_google_maps_api_key" value="'. get_option( 'msm_google_maps_api_key' ) .'" type="text" class="regular-text" />';
		}

		function msm_twitter_url_callback() {
		 echo '<input name="msm_twitter_url" id="msm_twitter_url" value="'. get_option( 'msm_twitter_url' ) .'" type="text" class="regular-text" />';
		}

		function msm_facebook_app_id_callback() {
		 echo '<input name="msm_facebook_app_id" id="msm_facebook_app_id" value="'. get_option( 'msm_facebook_app_id' ) .'" type="text" class="regular-text" />';
		}

		function msm_facebook_url_callback() {
		 echo '<input name="msm_facebook_url" id="msm_facebook_url" value="'. get_option( 'msm_facebook_url' ) .'" type="text" class="regular-text" />';
		}

		function msm_instagram_url_callback() {
		 echo '<input name="msm_instagram_url" id="msm_instagram_url" value="'. get_option( 'msm_instagram_url' ) .'" type="text" class="regular-text" />';
		}

		function msm_contact_email_callback() {
		 echo '<input name="msm_contact_email" id="msm_contact_email" value="'. get_option( 'msm_contact_email' ) .'" type="text" class="regular-text" />';
		}

		function msm_contact_phone_callback() {
		 echo '<input name="msm_contact_phone" id="msm_contact_phone" value="'. get_option( 'msm_contact_phone' ) .'" type="text" class="regular-text" />';
		}

		function msm_address_callback() {
		 echo '<input name="msm_address" id="msm_address" value="'. get_option( 'msm_address' ) .'" type="text" class="regular-text" />';
		}
?>
